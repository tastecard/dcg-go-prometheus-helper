# dcg-go-prometheus-helper

## Getting Started

On the cli run glide get;

```
glide get bitbucket.org/tastecard/dcg-go-prometheus-helper
```

## Using the package

Constructs the Metrics package to talk to a
prometheus gateway on http://0.0.0.0 on port 9091
The second arg is the wrapper for prometheus,
if you want to actually call out to the server, leave this as is.
If you want to change the functionality around calling out to the service, write your own wrapper.
Just make sure that it implements the PrometheusFunctionality interface

```
m := metrics.New("http://0.0.0.0:9091", mockPrometheusWrapper{})
```

To add a counter use the snippet below, swapping out to the correct values

```
m.AddCounter(prometheus.CounterOpts{
	Name: "input_counter",
	Help: "Input Counter",
})
```

To increment the previously created counter and send it to prometheus, recommended doing this as a go routine so
that the request to prometheus doesn't block;

```
go m.IncrementCounter("input_counter")
```

To make sure all metrics have finished sending to prometheus, run;
This will return a bool of either true or false.
This can be useful when in a procedural context to see if the go routines have finished running
```
m.AllMetricsSent()
```