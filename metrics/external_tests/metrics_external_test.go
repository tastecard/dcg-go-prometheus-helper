package externaltests

import (
	"bitbucket.org/tastecard/dcg-go-prometheus-helper/metrics"
	"github.com/prometheus/client_golang/prometheus"
	"testing"
	"time"
)

type mockPrometheusWrapper struct {
	FuncPushCollectors func (job string, grouping map[string]string, url string, collectors ...prometheus.Collector) error
}
func (m mockPrometheusWrapper) MustRegister (cs ...prometheus.Collector) {}
func (m mockPrometheusWrapper) PushCollectors (job string, grouping map[string]string, url string, collectors ...prometheus.Collector) error {
	if m.FuncPushCollectors == nil {
		return nil
	}
	return m.FuncPushCollectors(job, grouping, url, collectors...)
}
func (m mockPrometheusWrapper) HostnameGroupingKey () map[string]string {
	return make(map[string]string)
}

// TestAddCounterActsAsExpected tests that a counter is registered correctly
func TestAddCounterActsAsExpected (t *testing.T) {
	counterOpts := prometheus.CounterOpts{
		Name: "input_counter",
		Help: "Input Counter",
	}

	expectedCounter := prometheus.NewCounter(counterOpts)

	m := metrics.New("input_counter", mockPrometheusWrapper{})
	m.AddCounter(counterOpts)

	if m.CounterMetrics["input_counter"] == expectedCounter {
		t.Errorf(
			"Not setting correctly,\nexpecting:\n%+v,\noutput:\n%+v",
			expectedCounter,
			m.CounterMetrics["input_counter"],
		)
	}
}

// TestAllMetricsSentWorksWithGoRoutines tests that AllMetricsSent returns false when we are
// waiting for a job to complete
func TestAllMetricsSentWorksWithGoRoutines(t *testing.T) {
	counterOpts := prometheus.CounterOpts{
		Name: "input_counter",
		Help: "Input Counter",
	}

	// This function will be injected into the prometheus wrapper,
	// with a sleep to spoof a web request
	m := metrics.New("input_counter", mockPrometheusWrapper{
		FuncPushCollectors: func (job string, grouping map[string]string, url string, collectors ...prometheus.Collector) error {
			time.Sleep(time.Second * 3)
			return nil
		},
	})
	m.AddCounter(counterOpts)
	go m.IncrementCounter("input_counter")
	time.Sleep(time.Second * 1)

	completed := m.AllMetricsSent()

	if completed == true {
		t.Errorf("Not expecting all jobs to be completed.... they are")
	}

	time.Sleep(time.Second * 2)

	completed = m.AllMetricsSent()

	if completed == false {
		t.Errorf("expecting all jobs to be completed... they aren't")
	}
}
