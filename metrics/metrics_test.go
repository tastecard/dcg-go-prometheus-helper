package metrics

import (
	"errors"
	"github.com/prometheus/client_golang/prometheus"
	"testing"
)

type mockPrometheusWrapper struct {
	PushCollectorsValue error
}
func (m mockPrometheusWrapper) MustRegister (cs ...prometheus.Collector) {}

func (m mockPrometheusWrapper) PushCollectors (job string, grouping map[string]string, url string, collectors ...prometheus.Collector) error {
	if m.PushCollectorsValue == nil {
		return nil
	}
	return m.PushCollectorsValue
}

func (m mockPrometheusWrapper) HostnameGroupingKey () map[string]string {
	return make(map[string]string)
}

// TestAddCounterActsAsExpected tests that a counter is registered correctly
func TestAddCounterActsAsExpected (t *testing.T) {
	counterOpts := prometheus.CounterOpts{
		Name: "input_counter",
		Help: "Input Counter",
	}
	expectedCounter := prometheus.NewCounter(counterOpts)

	m := New("", mockPrometheusWrapper{})
	m.AddCounter(counterOpts)

	if m.CounterMetrics["input_counter"] == expectedCounter {
		t.Errorf(
			"Not setting correctly,\nexpecting:\n%+v,\noutput:\n%+v",
			expectedCounter,
			m.CounterMetrics["input_counter"],
		)
	}
}

// TestIncrementCounterReturnsAsExpected tests that IncrementCounter increments as expected
func TestIncrementCounterReturnsAsExpected (t *testing.T) {

	testCases := []struct{
		name string
		// input
		prometheusWrapper PrometheusFunctionality
		counterName string
		// expected
		expectedTotal int
		expectedCompleted int
	}{
		{
			"As expected",
			mockPrometheusWrapper{},
			"input_counter",
			1,
			1,
		},
		{
			"Unknown counter called",
			mockPrometheusWrapper{},
			"unknown",
			0,
			0,
		},
		{
			"No counter name called",
			mockPrometheusWrapper{},
			"",
			0,
			0,
		},
		{
			"Push collectors throws error",
			mockPrometheusWrapper{
				PushCollectorsValue: errors.New("some error"),
			},

			"input_counter",
			0,
			0,
		},
	}

	for _, tc := range testCases {

		expectedTotal := tc.expectedTotal
		expectedCompleted := tc.expectedCompleted
		counterOpts := prometheus.CounterOpts{
			Name: "input_counter",
			Help: "Input Counter",
		}
		m := New("http://127.0.0.1:9091/", tc.prometheusWrapper)
		m.AddCounter(counterOpts)
		m.IncrementCounter(tc.counterName)

		if expectedTotal != m.totalMetrics {
			t.Errorf("%s,\nexpected:%d,\noutput:%d", tc.name, expectedTotal, m.totalMetrics)
		}

		if expectedCompleted != m.completedMetrics {
			t.Errorf("%s,\nexpected:%d,\noutput:%d", tc.name, expectedCompleted, m.completedMetrics)
		}
	}

}

// TestAllMetricsSent tests that all metrics have been sent, return bool
// the idea being that you may choose to do something if all metrics have not been sent
func TestAllMetricsSent (t *testing.T) {

	counterOpts := prometheus.CounterOpts{
		Name: "input_counter",
		Help: "Input Counter",
	}

	testCases := []struct{
		name string
		// input
		prometheusWrapper PrometheusFunctionality
		incrementCounter bool
		// expected
		expectedOutcome bool
	}{
		{
			"All metrics sent successfull",
			mockPrometheusWrapper{},
			true,
			true,
		},
		// When an error is thrown, we want the totalMetrics to rollback as the jobs has not been successful,
		// but we do want to say that the job has completed
		{
			"All metrics sent event when error thrown",
			mockPrometheusWrapper{
				PushCollectorsValue: errors.New("some error"),
			},
			true,
			true,
		},
		{
			"All metrics not sent when not send metrics",
			mockPrometheusWrapper{
				PushCollectorsValue: errors.New("some error"),
			},
			true,
			true,
		},
	}

	for _, tc := range testCases {
		m := New("http://0.0.0.0:9091/", tc.prometheusWrapper)
		m.AddCounter(counterOpts)
		m.IncrementCounter(counterOpts.Name)

		outcome := m.AllMetricsSent()

		if tc.expectedOutcome != outcome {
			t.Errorf(
				"%s,\nunexpected result returned,\nexpected:\n%+v,\noutput:\n%+v,\ntotalMetrics: %d, completedMetrics: %d",
				tc.name,
				tc.expectedOutcome,
				outcome,
				m.totalMetrics,
				m.completedMetrics,
				)
		}
	}
}