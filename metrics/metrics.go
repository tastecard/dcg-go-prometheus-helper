package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/push"
)

// PrometheusFunctionality is the interface which fires requests off to promethsus
// this is here to allow you to switch out this functionality with a mock
// or to alter the way that metrics speaks to your prometheus
type PrometheusFunctionality interface {//TODO rename
	MustRegister (cs ...prometheus.Collector)
	PushCollectors (job string, grouping map[string]string, url string, collectors ...prometheus.Collector) error
	HostnameGroupingKey () map[string]string
}

// PrometheusWrapper is what is used in Metrics.PrometheusWrapper to fire off requests, this can
// be replaced but it must fullfill the above interface
type PrometheusWrapper struct {}

// MustRegister registers the provided Collectors with the DefaultRegisterer
func (p PrometheusWrapper) MustRegister (cs ...prometheus.Collector) {
	prometheus.MustRegister(cs...)
}

// PushCollectors works like FromGatherer, but it does not use a Gatherer. Instead,
// it collects from the provided collectors directly. It is a convenient way to
// push only a few metrics.
func (p PrometheusWrapper) PushCollectors (job string, grouping map[string]string, url string, collectors ...prometheus.Collector) error {
	err := push.Collectors(
		"signups", push.HostnameGroupingKey(),
		url, collectors...,
	)
	return err
}

// HostnameGroupingKey returns a label map with the only entry
// {instance="<hostname>"}. This can be conveniently used as the grouping
// parameter if metrics should be pushed with the hostname as label. The
// returned map is created upon each call so that the caller is free to add more
// labels to the map.
func (p PrometheusWrapper) HostnameGroupingKey () map[string]string {
	return push.HostnameGroupingKey()
}

// Metrics the main struct which will be used to implement this functionality
type Metrics struct {
	Endpoint string
	CounterMetrics map[string]prometheus.Counter
	PrometheusWrapper PrometheusFunctionality
	
	totalMetrics int
	completedMetrics int
}

// New constructor method
func New(endpoint string, promWrapper PrometheusFunctionality) Metrics {
	return Metrics{
		Endpoint: endpoint,
		CounterMetrics: make(map[string]prometheus.Counter),
		PrometheusWrapper: promWrapper,
	}
}

// AddCounter registers the given counter with prometheus
func (m *Metrics) AddCounter (co prometheus.CounterOpts) {
	m.CounterMetrics[co.Name] = prometheus.NewCounter(co)
	m.PrometheusWrapper.MustRegister(m.CounterMetrics[co.Name])
}

// IncrementCounter increments the counter with the name provided //TODO add incrmenting jobs
func (m *Metrics) IncrementCounter (name string) {

	if m.CounterMetrics[name] == nil {
		return //TODO throw some kind of error, but will be go routine
	}
	m.totalMetrics++

	err := m.PrometheusWrapper.PushCollectors(name,
		m.PrometheusWrapper.HostnameGroupingKey(),
		m.Endpoint,
		m.CounterMetrics[name],
	)


	if err != nil {
		m.totalMetrics--
	} else {
		m.completedMetrics++
	}
}

// AllMetricsSent returns bool to whether all metrics have been sent and received
// this will allow you to check all the metrics have been sent before doing something else in your code
func (m Metrics) AllMetricsSent () bool {
	return m.completedMetrics == m.totalMetrics
}